/**
 * Created by Administrator on 2016-07-29.
 */
/**
 * qrCode 팝업 창
 * @param path
 */
function qrOpen(path){
    cw=screen.availWidth;     //화면 넓이
    ch=screen.availHeight;    //화면 높이

//            sw=500;    //띄울 창의 넓이
    sw=390;
    sh=430;

    ml=(cw-sw)/2;        //가운데 띄우기위한 창의 x위치
    mt=(ch-sh)/2;         //가운데 띄우기위한 창의 y위치

    test=window.open(path,'tst','width='+sw+',height='+sh+',top='+mt+',left='+ml+',resizable=no,scrollbars=yes, location=1');
}

/**
 * viewer팝업 창
 * @param path
 */
function popupOpen(path){
    cw=screen.availWidth;     //화면 넓이
    ch=screen.availHeight;    //화면 높이

    sw=1000;    //띄울 창의 넓이
    sh=$(window).width();    //띄울 창의 높이

    ml=(cw-sw)/2;        //가운데 띄우기위한 창의 x위치
    mt=(ch-sh)/2;         //가운데 띄우기위한 창의 y위치

    test=window.open(path,'tst','width='+sw+',height='+sh+',top='+mt+',left='+ml+',resizable=no,scrollbars=yes, location=1');
}

/**
 * 로그인 필터링
 */
window.onload = function() {
    if(location.pathname != "/notnull/iwbook/index.html"){
        if(sessionStorage["loginId"]==null){
            // location.href = "index.html";
            location.href = getContextPath() + "/iwbook/index.html";
        }
    }
};
/**
 * Context경로 구하기 'http://localhost:9000/notnull/'
 * @returns {string}
 */
function getContextPath() {
    var hostIndex = location.href.indexOf( location.host ) + location.host.length;
    return location.href.substring( 0, location.href.indexOf('/', hostIndex + 1) );
};

/**
 * 로그 아웃 테스트
 */
function logOutTest(){
    sessionStorage.clear();
    location.href = getContextPath() + "/iwbook/index.html";
}
